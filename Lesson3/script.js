'use strict';

let money, time;

function start() {
    money = "";
    time = prompt("Введите дату в формате YYYY-MM-DD", "2020-20-12");
    
    while (isNaN(money) || money == "" || money == null) {
        money = +prompt("Ваш бюджет на месяц?", "0");    
    }
}

function chooseExpenses() {

    for (let i = 0; i < 2; i++) {
        let a = prompt("Введите обязательную статью расхода в этом месяце", "Жена"),
            b = +prompt("Во сколько обойдется?", "0");
        if (typeof(a) === 'string' && typeof(a) != null && typeof(b) != null && 
            a != "" && b != "" && a.length < 50) {
            appData.expenses[a] = b;
        } else {
            i = i - 1;
        }
    }
    
}

function checkSavings() {
    if (appData.savings == true) {
        let save = +prompt("Какова сумма накоплений?", ""),
            percent = +prompt("Какой процент?", "");
        appData.monthIncome = save / 100 / 12 * percent;
        alert("Доход в месяц с вашего депозита " + appData.monthIncome);
    }
}

function detectDayBudged() {
    appData.moneyPerDay = (appData.budget / 30).toFixed();
    alert('Ежедневный бюджет: ' + appData.moneyPerDay);
}

function detectLevel() {
    if (appData.moneyPerDay < 100) {
        console.log("Минимальный уровень достатка");
    } else if (appData.moneyPerDay < 2000) {
        console.log("Средний уровень достатка");
    } else if (appData.moneyPerDay > 2000) {
        console.log("Высокий уровень достатка");
    } else {
        console.log("Error");
    }
}

function chooseOptExpenses() {
    for (let i = 0; i < 3; i++) {
        let expense = prompt("Статья необязательных расходов?", "");
        appData.optionalExpenses[i] = expense;
    }
}

start();

let appData = {
    budget: money,
    timeData: time,
    expenses: {},
    optionalExpenses: {},
    income: [],
    savings: true
};

chooseExpenses();

detectDayBudged();

detectLevel();

checkSavings();